# frozen_string_literal: true

RSpec.describe RuboCop::Cop::Database::ConcurrentIndex do
  subject(:cop) { described_class.new }

  context 'without the concurrent option' do
    it 'registers an offense without options' do
      inspect_source(<<~SOURCE)
        add_index(:products, :name)
        drop_index(:products, :name)
        add_reference(:products, :name, foreign_key: true)
        remove_reference(:products, :name, foreign_key: true)
      SOURCE
      expect(cop.offenses.size).to eq(4)
    end

    it 'registers an offense without options' do
      inspect_source(<<~SOURCE)
        add_reference(:products, :name)
        remove_reference(:products, :name)
      SOURCE
      expect(cop.offenses.size).to eq(2)
    end

    it 'registers an offense with other options' do
      inspect_source(<<~SOURCE)
        add_index(:products, :name, unique: true)
        drop_index(:products, :name, unique: true)
      SOURCE
      expect(cop.offenses.size).to eq(2)
    end

    it 'registers an offense with composite index' do
      inspect_source(<<~SOURCE)
        add_index(:products, [:name, :price], unique: true)
        drop_index(:products, [:name, :price])
      SOURCE
      expect(cop.offenses.size).to eq(2)
    end
  end

  it 'does not register an offense when using concurrent option' do
    inspect_source(<<~SOURCE)
      add_index(:products, :name, unique: true, concurrently: true)
      drop_index(:products, :name, concurrently: true)
    SOURCE
    expect(cop.offenses).to be_empty
  end

  it 'does not register an offense when using concurrent option' do
    inspect_source(<<~SOURCE)
      add_reference(:products, :name, foreign_key: true, index: {algorithm: :concurrently})
      remove_reference(:products, :name, foreign_key: true, index: {algorithm: :concurrently})
    SOURCE
    expect(cop.offenses).to be_empty
  end
end
