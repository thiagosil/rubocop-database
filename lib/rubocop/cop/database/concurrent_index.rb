# frozen_string_literal: true

# TODO: when finished, run `rake generate_cops_documentation` to update the docs
module RuboCop
  module Cop
    module Database
      # Prevents synchronous index creation blocking the database.
      #
      #   # bad
      #   add_index(:products, :name)
      #
      #   # bad
      #   add_reference(:products, :name)
      #
      #   # good
      #   add_index(:products, :name, unique: true, concurrently: true)
      #
      #   # good
      #   add_reference(:products, :name, foreign_key: true, index: {algorithm: :concurrently})
      class ConcurrentIndex < Cop
        MSG = 'Prefer creating or dropping new index concurrently'

        def_node_matcher :indexes?, <<-MATCHER
          (send _ {:add_index :drop_index :add_reference :remove_reference} $...)
        MATCHER

        def on_send(node)
          indexes?(node) do |args|
            if offensive?(args)
              add_offense(node, location: :selector, message: MSG)
            end
          end
        end

        private

        def offensive?(args)
          !args.last.hash_type? || args.last.each_descendant.none? do |n|
            next unless n.sym_type?

            n.children.any? { |s| s == :concurrently }
          end
        end
      end
    end
  end
end
