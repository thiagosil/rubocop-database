# frozen_string_literal: true

require 'rubocop'

require_relative 'rubocop/database'
require_relative 'rubocop/database/version'
require_relative 'rubocop/database/inject'

RuboCop::Database::Inject.defaults!

require_relative 'rubocop/cop/database_cops'
